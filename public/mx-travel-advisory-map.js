$(document).ready(function() {
  ///////////////////////// MAKE A MAP /////////////////////////
  var width = 680,
      height = 550;

  // Reference of projection: https://bl.ocks.org/mbostock/9265674
  var projection = d3.geo.conicConformal()
      .rotate([102, 0])
      .center([0, 24])
      .parallels([17.5, 29.5])
      .scale(1350)
      .translate([width / 2, height / 2]);

  var path = d3.geo.path()
    .projection(projection);

  var svg = d3.select('#map').append('svg')
        .attr('width', width)
        .attr('height', height);

  var map = svg.append('g');
  var mx_g = map.append('g');

  d3.json('mx.geojson', function(error, mx) {
    if (error) return console.error(error);
    console.log(mx);
    var states = mx.features;
    $('#loading-text').hide();
    $('#interactive-container').show();
    $('#interactive-container').attr('display', 'flex');
    $('#hover-directions').show();

    mx_g.selectAll('path')
        .data(states)
        .enter()
        .append('path')
        .attr('d', path)
        .attr('id', function(d) { return d.properties.CVE_ENT })
        .attr('class', function(d) { return 'lvl-' + d.properties.TRV_ADV });

    ///////////////////////// LEGEND /////////////////////////
    // Reference: http://d3-legend-v3.susielu.com/#color-ordinal
    var ordinal = d3.scale.ordinal()
    .domain([ "1 - Exercise normal precautions",
              "2 - Exercise increased caution",
              "3 - Reconsider travel",
              "4 - Do not travel"])
    .range([ "#003875", "#ffcc66", "#ff9900", "#ff0000"]);

    svg.append("g")
    .attr("class", "legendOrdinal")
    .attr("transform", "translate(450,20)");

    var legendOrdinal = d3.legend.color()
    .shape("path", d3.svg.symbol().type("circle").size(150)())
    .shapePadding(10)
    .scale(ordinal)
    .title("Travel Advisory Levels")
    .on('cellover', function(d) {
      var lvl = d.charAt(0);
      $('.lvl-' + lvl).addClass('shade');
      $('#list-' + lvl).show();
      $('#hover-directions').hide();
      d.style('fill', '#ccc');
    }).on('cellout', function(d) {
      var lvl = d.charAt(0);
      $('.lvl-' + lvl).removeClass('shade');
      $('#list-' + lvl).hide();
      $('#hover-directions').show();
      d.style('fill', '#fffdf5');
    });

    svg.select(".legendOrdinal")
    .style('fill', '#fffdf5')
    .style("font-size","11px")
    .call(legendOrdinal);

    ///////////////////////// DISPLAY INFO ON HOVER /////////////////////////
    $('path').hover(
      function() {    // mouseEnter
        console.log('enter');
        $('#hover-directions').hide();
        $('#advisory-' + this.id).show();
      }, function() { // mouseLeave
        console.log('leave');
        $('#advisory-' + this.id).hide();
        $('#hover-directions').show();
      }
    );
  });
});
