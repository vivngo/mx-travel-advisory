# Process: Mexico Travel Advisory Warning Map

Got shapefiles:
```bash
curl -o estados.zip http://mapserver.inegi.org.mx/MGN/mge2010v5_0.zip
unzip estados.zip
```

Followed along with [this map](https://bl.ocks.org/mbostock/9265674) to learn D3.js

Made a  file by hand of all the travel warnings for each state from the
[DOS website](https://travel.state.gov/content/travel/en/international-travel/International-Travel-Country-Information-Pages/Mexico.html)

Used QGIS to combine shapefiles with column for travel warnings from CSV file.

Exported as a geojson file to use in D3.js

Lots of D3.js and CSS+Javascript+jQuery to have nice hovering functionality :-)

Used Susie Lu's [d3-legend](http://d3-legend-v3.susielu.com/) library to have a legend. Try hovering over the legend!
